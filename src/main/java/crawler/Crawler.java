package crawler;

/**
 * Created by Jimmy on 24/10/15.
 */

public interface Crawler {
    String[] crawl(final String url);
}
