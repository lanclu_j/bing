package crawler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.*;

/**
 * Created by Jimmy on 24/10/15.
 */
public class BasicCrawler implements Crawler {

    private static final Logger Log = LogManager.getLogger(BasicCrawler.class);
    private static final int DEPTH = 0;
    private final Set<String> data;

    public BasicCrawler() {
        data = new HashSet<>();
    }

    @Override
    public String[] crawl(final String url) {
        try {
            new HashSet<>();

            crawl(url, DEPTH);

            return data.toArray(new String[data.size()]);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return new String[0];
    }

    private void crawl (final String url, int depth) throws IOException {

        try {
            Document doc = Jsoup.connect(url).get();
            Elements links = doc.select("a[href]");
            data.add(url);
            links.forEach(link -> {
                String strLink = link.attr("href");
                if (!data.contains(strLink) && isUrlValid(strLink)) {
                    if (depth > 0)
                        try {
                            crawl(strLink, depth - 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                }
            });
        }
        catch (IllegalArgumentException | HttpStatusException | SocketTimeoutException ex) {
            Log.error(ex.getMessage());
        }
    }

    private boolean isUrlValid(String url) {
        try {
            new URL(url);

            return url.startsWith("http://");
        }
        catch (Exception ex) {
            return false;
        }
    }
}
