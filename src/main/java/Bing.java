import crawler.BasicCrawler;
import crawler.Crawler;
import indexer.BasicIndexer;
import indexer.Indexer;
import indexer.LocalIndex;
import indexer.retro.BasicRetroIndexer;
import indexer.retro.RetroIndexer;
import indexer.model.SearchResult;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jimmy on 24/10/15.
 */
public class Bing {


    final static String urlToCrawl = "http://snowball.tartarus.org/algorithms/english/stemmer.html";
    final static String wordToSearch = "consist";

    public static void main(String... args) {

        Crawler crawler = new BasicCrawler();
        String[] webPageCrawled = crawler.crawl(urlToCrawl);

        Indexer indexer = new BasicIndexer();
        Set<LocalIndex> localIndexes = new HashSet<>();

        String url = webPageCrawled[0];

        String pageContentCleaned = indexer.cleanUp(getPageContent(url));
        String[] tokens = indexer.tokenize(pageContentCleaned);
        String[] tokenReduiced = indexer.reduice(tokens);

        LocalIndex index = indexer.value(tokenReduiced);
        index.setUrl(urlToCrawl);

        RetroIndexer retroIndexer = new BasicRetroIndexer();
        retroIndexer.addEntry(index);

        SearchResult sr = retroIndexer.find(wordToSearch);
    }

    private static String getPageContent(String url) {
        try {
            return Jsoup.connect(url).get().body().text();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
