package indexer;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * Created by Jimmy on 24/10/15.
 */
@Getter
@Setter
public class FrequencyPosition {
    Set<Integer> positions;
    Double frequency;
}
