package indexer;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.stream.Stream;

/**
 * Created by Jimmy on 24/10/15.
 */
@Getter
@Setter
public class LocalIndex {

    private HashMap<String, FrequencyPosition> frequencyPositionHashMap;

    private String url;

    private HashMap<String, Double> tdidf;
    private double norm;

    public LocalIndex() {
        frequencyPositionHashMap = new HashMap<>();
    }

    private double getNorm() { return norm; }
    private void setNorm(double norm) {
        this.norm = norm;
    }

    public void calcFrequencies() {
        Stream<FrequencyPosition> stream = frequencyPositionHashMap.keySet()
                .stream()
                .map(frequencyPositionHashMap::get);


        norm = stream.mapToDouble(value -> value.getPositions().size())
                .reduce(0, (a, b) -> Math.pow(a, 2) + Math.pow(b, 2));

        stream.forEach(fp -> fp.setFrequency(fp.getPositions().size() / norm));
    }

}
