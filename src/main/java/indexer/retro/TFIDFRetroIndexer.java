package indexer.retro;

import indexer.FrequencyPosition;
import indexer.LocalIndex;
import indexer.model.FrequencyPositionUrl;
import indexer.model.IndexedFrequenciesPositionsUrls;
import indexer.model.SearchResult;
import lombok.Getter;

import java.util.*;

/**
 * Created by Jimmy on 06/11/15.
 */
@Getter
public class TFIDFRetroIndexer implements RetroIndexer {

    HashMap<String, IndexedFrequenciesPositionsUrls> searchMap;
    int nbDocuments = 0;

    TFIDFRetroIndexer() {
        searchMap = new HashMap<>();

    }

    @Override
    public void addEntry(final LocalIndex document) {
        nbDocuments++;
        document.getFrequencyPositionHashMap().keySet().stream().forEach(word -> {

            FrequencyPositionUrl frequencyPositionUrl;
            if (getSearchMap().containsKey(word)) {
                ArrayList<FrequencyPositionUrl> frequencyPositionUrls = getSearchMap().get(word).getFrequencyPositionUrls();

                FrequencyPosition fp = document.getFrequencyPositionHashMap().get(word);
                Double wordFrequency = fp.getFrequency();
                wordFrequency += document.getFrequencyPositionHashMap().get(word).getFrequency();
                fp.setFrequency(wordFrequency);

                FrequencyPositionUrl fpu = new FrequencyPositionUrl();
                fpu.setFrequencyPosition(fp);
                fpu.setUrl(document.getUrl());

                frequencyPositionUrls.add(fpu);
            } else {
                frequencyPositionUrl = new FrequencyPositionUrl();

                frequencyPositionUrl.setUrl(document.getUrl());
                frequencyPositionUrl.setFrequencyPosition(document.getFrequencyPositionHashMap().get(word));

                ArrayList<FrequencyPositionUrl> frequencyPositionUrls = new ArrayList<>();
                frequencyPositionUrls.add(frequencyPositionUrl);

                IndexedFrequenciesPositionsUrls indexedFrequenciesPositionsUrls = new IndexedFrequenciesPositionsUrls();
                indexedFrequenciesPositionsUrls.setFrequencyPositionUrls(frequencyPositionUrls);
                getSearchMap().put(word, indexedFrequenciesPositionsUrls);
            }
        });

    }

    @Override
    public SearchResult find(String search) {
        SearchResult sr = new SearchResult();
        List<String> query = Arrays.asList(search.split(" "));
        ArrayList<IndexedFrequenciesPositionsUrls> matches = sr.getResult();

        searchMap.keySet().stream().filter(query::contains).forEach(word -> matches.add(searchMap.get(word)));

        matches.sort((o1, o2) -> Double.compare(o1.getTfidf(), o2.getTfidf()));

        CalcTfIdfScore();

        return sr;
    }

    public void CalcTfIdfScore() {
        searchMap.keySet().stream().forEach(word -> {
            double wordFrequencyInDocuments = searchMap.get(word).getFrequencyPositionUrls().size();
            searchMap.get(word).setTfidf(Math.log(getNbDocuments() / wordFrequencyInDocuments));
        });
    }
}
