package indexer.retro;

import indexer.LocalIndex;
import indexer.model.FrequencyPositionUrl;
import indexer.model.SearchResult;

import java.util.HashMap;

/**
 * Created by Jimmy on 24/10/15.
 */
public interface RetroIndexer {

    HashMap<String, FrequencyPositionUrl> getSearchMap();

    void addEntry(final LocalIndex document);

    SearchResult find(final String search);
}
