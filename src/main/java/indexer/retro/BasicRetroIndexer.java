package indexer.retro;

import indexer.FrequencyPosition;
import indexer.LocalIndex;
import indexer.model.FrequencyPositionUrl;
import indexer.model.SearchResult;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

/**
 * Created by Jimmy on 06/11/15.
 */
@Getter
@Deprecated
@Setter
public class BasicRetroIndexer implements RetroIndexer {

    // mot <url, freq_pos>
    HashMap<String, FrequencyPositionUrl> searchMap;

    public BasicRetroIndexer() {
        searchMap = new HashMap<>();
    }

    @Override
    public void addEntry(final LocalIndex document) {
        document.getFrequencyPositionHashMap().keySet().stream().forEach(word -> {

            FrequencyPositionUrl frequencyPositionUrl;
            if (getSearchMap().containsKey(word)) {
                frequencyPositionUrl = getSearchMap().get(word);

                FrequencyPosition fp = frequencyPositionUrl.getFrequencyPosition();
                Double wordFrequency = fp.getFrequency();
                wordFrequency += document.getFrequencyPositionHashMap().get(word).getFrequency();
                fp.setFrequency(wordFrequency);
            } else {
                frequencyPositionUrl = new FrequencyPositionUrl();

                frequencyPositionUrl.setUrl(document.getUrl());
                frequencyPositionUrl.setFrequencyPosition(document.getFrequencyPositionHashMap().get(word));
                getSearchMap().put(word, frequencyPositionUrl);
            }
        });

    }

    @Override
    public SearchResult find(String search) {
        SearchResult sr = new SearchResult();

        //searchMap.keySet().stream().filter(search::contains).forEach(match -> sr.getResult().add(searchMap.get(match)));

        return sr;
    }
}
