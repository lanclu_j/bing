package indexer.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Jimmy on 13/11/15.
 */
@Getter
@Setter
public class IndexedFrequenciesPositionsUrls {
    ArrayList<FrequencyPositionUrl> frequencyPositionUrls;
    private double tfidf;
}
