package indexer.model;

import indexer.model.IndexedFrequenciesPositionsUrls;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Jimmy on 24/10/15.
 */
@Getter
@Setter
public class SearchResult {
    ArrayList<IndexedFrequenciesPositionsUrls> result;

    private String query;

    public SearchResult() {
        result = new ArrayList<>();
    }
}
