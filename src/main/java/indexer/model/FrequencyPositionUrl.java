package indexer.model;

import indexer.FrequencyPosition;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Jimmy on 06/11/15.
 */
@Getter
@Setter
public class FrequencyPositionUrl {
    private String url;
    private FrequencyPosition frequencyPosition;
}
