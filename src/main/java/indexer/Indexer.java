package indexer;

/**
 * Created by Jimmy on 24/10/15.
 */
public interface Indexer {
    String cleanUp(String input);
    String[] tokenize(String input);
    String[] reduice(String[] input);
    LocalIndex value(String[] input);
}
